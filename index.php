<?php
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "home";
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8" />
    <title>TEDx Montebelluna</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna" />
    <meta name="keywords" content="tedx montebelluna, ted montebelluna" />
    <meta property="og:title" content="TEDxMontebelluna, evento TED organizzato in modo indipendente." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.tedxmontebelluna.com" />
    <meta property="og:locale" content="it_IT" />

    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ff2b06">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ff2b06">

    <link href="https://fonts.googleapis.com/css?family=Cantarell:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.min.css?<?php echo time(); ?>" rel="stylesheet" type="text/css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="page container-fluid">
        <header class="container">
            <div class="row">
                <div class="col-md-11 ted-header-left">
                    <div class="ted-logo">
                        <a href="home"><img height="60" src="images/logo_white.png" alt="Logo TedX Montebelluna" /></a>
                    </div>
                </div>
                <div class="col-md-1 ted-header-right">
                    <a href="javascript: showMenu();">
                        <div class="ted-menu">
                            <i class="fa fa-bars"></i>
                        </div>
                    </a>
                </div>
            </div>
        </header>

        <?php
include $page . ".php";
?>

        <footer>
            <div class="container ted-footer-menu">
                <ul>
                    <li><a href="home">Home</li>
                    <li><a href="#">Speaker</li>
                    <li><a href="#">Partner</li>
                    <li><a href="#">About Ted</li>
                    <li><a href="team">Chi siamo</li>
                    <li><a href="#">Contatti</li>
                </ul>
            </div>
            <div class="container ted-footer-contacts">
                <p class="ted-footer-social">
                    <a href="#"><i class="fa fa-facebook"></i></a> &nbsp; <a href="#"><i class="fa fa-instagram"></i></a> &nbsp; <a href="#"><i class="fa fa-vimeo"></i></a> &nbsp; <a href="#"><i class="fa fa-youtube"></i></a>
                </p>
                <p>Comitato Promotore TEDxMontebelluna<br />
                    Via non so, 00, Montebelluna (TV) 31044<br />
                    <a href="mailto:info@tedxmontebelluna.com">info@tedxmontebelluna.com</a>
                </p>
            </div>
            <div class="ted-credits">
                <div class="row">
                    <div class="col-md-4 text-left">
                        © TEDxMontebelluna - All rights reserved
                    </div>
                    <div class="col-md-4 text-center">
                        This independent TEDx event is operated under license from TED.
                    </div>
                    <div class="col-md-4 text-right">
                        designed by <a target="_blank" href="#">Maremoto Studio</a> - developed by <a target="_blank" href="https://www.igsolutions.it">IG Solutions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="ted-fixed-menu" id="ted_fixed_menu">
        <div class="ted-fixed-menu-close">
            <a href="javascript: hideMenu();"><i class="fa fa-times"></i></a>
        </div>
        <div class="ted-fixed-menu-table-layout">
            <div class="ted-fixed-menu-list">
                <ul>
                    <li><a href="home">Home</li>
                    <li><a href="#">Speaker</li>
                    <li><a href="#">Partner</li>
                    <li><a href="#">About Ted</li>
                    <li><a href="team">Chi siamo</li>
                    <li><a href="#">Contatti</li>
                </ul>
            </div>
        </div>
    </div>

    <script src="js/main.min.js"></script>

    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109550788-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109550788-1');
        </script>
    <?php endif;?>

</body>

</html>
