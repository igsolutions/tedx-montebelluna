<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8" />
    <title>TEDx Montebelluna</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna" />
    <meta name="keywords" content="tedx montebelluna, ted montebelluna" />
    <meta property="og:title" content="TEDxMontebelluna, evento TED organizzato in modo indipendente." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.tedxmontebelluna.com" />
    <meta property="og:locale" content="it_IT" />

    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ff2b06">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ff2b06">

    <link href="css/waiting.min.css?2" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="page">
        <header>
            <div class="logo">
                <a href="home"><img src="images/logo.png" alt="Logo TedX Montebelluna" /></a>
            </div>
        </header>

        <div class="construction">
            <div class="inner-container">
                <img src="images/construction.png" width="100%" />
            </div>
        </div>

        <footer>
            <div class="social">
                <a href="https://www.facebook.com/tedxmontebelluna/" target="_blank"><img src="images/fb.png" width="30" /></a> &nbsp; <a href="https://www.instagram.com/tedxmontebelluna/" target="_blank"><img src="images/instagram.png" width="30" /></a> &nbsp; <a href="https://twitter.com/TEDxMBL" target="_blank"><img src="images/twitter.png" width="30" /></a>
            </div>
        </footer>
    </div>

    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109550788-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109550788-1');
        </script>
    <?php endif; ?>

</body>

</html>
