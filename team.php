<style>
footer {
    border-top: none;
}
</style>
<div class="row" style="position: relative;">
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_ALBERTO"><img src="images/team/black/ALBERTO.jpg" width="100%" id="image_ALBERTO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ALBERTO">
                    <div class="team-fast-hover">
                        <h1>Alberto Moretto</h1>
                        <h2>Web developer</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ALBERTO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_ANDREA"><img src="images/team/black/ANDREA.jpg" width="100%" id="image_ANDREA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ANDREA">
                    <div class="team-fast-hover">
                        <h1>Andrea Franchin</h1>
                        <h2>Licenziatario e organizzatore</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ANDREA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_BEATRICE"><img src="images/team/black/BEATRICE.jpg" width="100%" id="image_BEATRICE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BEATRICE">
                    <div class="team-fast-hover">
                        <h1>Beatrice Pesente</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BEATRICE">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_DARIO"><img src="images/team/black/DARIO.jpg" width="100%" id="image_DARIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DARIO">
                    <div class="team-fast-hover">
                        <h1>Dario Merlo</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DARIO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_ELENA"><img src="images/team/black/ELENA.jpg" width="100%" id="image_ELENA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ELENA">
                    <div class="team-fast-hover">
                        <h1>Elena Bressan</h1>
                        <h2>Team speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ELENA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_EMILIANO"><img src="images/team/black/EMILIANO.jpg" width="100%" id="image_EMILIANO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_EMILIANO">
                    <div class="team-fast-hover">
                        <h1>Emiliano Guerra</h1>
                        <h2>Team speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="EMILIANO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_ENRICO"><img src="images/team/black/ENRICO.jpg" width="100%" id="image_ENRICO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ENRICO">
                    <div class="team-fast-hover">
                        <h1>Enrico Poloni</h1>
                        <h2>Team partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ENRICO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_FABIO"><img src="images/team/black/FABIO.jpg" width="100%" id="image_FABIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FABIO">
                    <div class="team-fast-hover">
                        <h1>Fabio Donà</h1>
                        <h2>Team partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FABIO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_FEDERICA"><img src="images/team/black/FEDERICA.jpg" width="100%" id="image_FEDERICA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FEDERICA">
                    <div class="team-fast-hover">
                        <h1>Federica Favero</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FEDERICA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_FRANCESCA"><img src="images/team/black/FRANCESCA.jpg" width="100%" id="image_FRANCESCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCA">
                    <div class="team-fast-hover">
                        <h1>Francesca Celato</h1>
                        <h2>Team speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_FRANCESCO"><img src="images/team/black/FRANCESCO.jpg" width="100%" id="image_FRANCESCO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCO">
                    <div class="team-fast-hover">
                        <h1>Francesco Calzamatta</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_GIANLUCA"><img src="images/team/black/GIANLUCA.jpg" width="100%" id="image_GIANLUCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GIANLUCA">
                    <div class="team-fast-hover">
                        <h1>Gianluca Innocente</h1>
                        <h2>Web developer</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GIANLUCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_MYRNA"><img src="images/team/black/MYRNA.jpg" width="100%" id="image_MYRNA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_MYRNA">
                    <div class="team-fast-hover">
                        <h1>Myrna Isetta</h1>
                        <h2>Team speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="MYRNA">

                </div>
            </div>
        </a>
    </div>
    <div class="team-popup-background" id="team_popup" style="display: none;">
        <div class="team-popup-container">
            <div class="col-container">
                <div class="col" style="width: 40%;">
                    <img src="images/team/x/ANDREA.PNG" width="100%" id="team_popup_image" />
                </div>
                <div class="col" style="width: 60%;">
                    <div class="team-popup-info">
                        <div>
                            <h1 id="team_popup_title">Andrea Franchin</h1>
                            <h2 id="team_popup_subtitle">Licenziatario e organizzatore</h2>
                            <p id="team_popup_description">Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è. </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-popup-left">
                <a href="#" id="team_popup_previous"></a>
            </div>

            <div class="team-popup-right">
                <a href="#" id="team_popup_next"></a>
            </div>

            <div class="team-popup-close">
                <a href="javascript:  $('#team_popup').fadeOut();"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
</div>
<script>
    var team = [
        {
            id: "ALBERTO",
            name: "Alberto Moretto",
            role: "Web Developer",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "ANDREA",
            name: "Andrea Franchin",
            role: "Licenziatario e organizzatore",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "BEATRICE",
            name: "Beatrice Pesente",
            role: "Comunicazione",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "DARIO",
            name: "Dario Merlo",
            role: "Comunicazione",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "ELENA",
            name: "Elena Bressan",
            role: "Team speaker",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "EMILIANO",
            name: "Emiliano Guerra",
            role: "Team speaker",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "ENRICO",
            name: "Enrico Poloni",
            role: "Team partner",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "FABIO",
            name: "Fabio Donà",
            role: "Team partner",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "FEDERICA",
            name: "Federica Favero",
            role: "Comunicazione",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "FRANCESCA",
            name: "Francesca Celato",
            role: "Team speaker",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "FRANCESCO",
            name: "Francesco Calzamatta",
            role: "Comunicazione",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "GIANLUCA",
            name: "Gianluca Innocente",
            role: "Web Developer",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        },
        {
            id: "MYRNA",
            name: "Myrna Isetta",
            role: "Team speaker",
            description: "Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è."
        }
    ];

    $(document).ready(function() {
        $(".team-hover").hover(function() {
            var element = $("#hover_content_" + this.id);
            var display = element.css("display");

            console.log("display "+display+"  for id "+this.id);

            if(display == 'none') {
                $("#image_"+this.id).attr("src", "images/team/colors/" + this.id + ".jpg");
                element.show();
                console.log("show "+this.id);
            }
        }, function() {
            var display = this.style.display;

            console.log("hide "+display+"  id "+this.id);

            $("#image_"+this.id).attr("src", "images/team/black/" + this.id + ".jpg");
            $("#hover_content_" + this.id).hide();
        });

        $(".team-hover").click(function() {
            var userID = this.id.replace("link_", "");
            console.log("open details for " + userID);

            show(userID);
        });
    });

    function show(userID) {
        $("#team_popup").fadeOut(200, function() {
            var userIndex = getUserIndexById(userID);
            var user = team[userIndex];
            var prevUser = userIndex > 0 ? team[userIndex - 1] : team[team.length - 1];
            var nextUser = userIndex < team.length - 1 ? team[userIndex + 1] : team[0];

            $("#team_popup_image").attr("src", "images/team/x/" + userID + ".png");
            $("#team_popup_title").html(user.name);
            $("#team_popup_subtitle").html(user.role);
            $("#team_popup_description").html(user.description);

            $("#team_popup_previous").html("< " + prevUser.id);
            $("#team_popup_previous").attr("href", "javascript: show('" + prevUser.id + "');");
            $("#team_popup_next").html(nextUser.id + " >");
            $("#team_popup_next").attr("href", "javascript: show('" + nextUser.id + "');");

            $("#team_popup").fadeIn(200);
        });
    }

    function getUserIndexById(userID) {
        var usrIndex = null;

        for(var i = 0; i < team.length; i++) {
            if(team[i].id == userID) {
                usrIndex = i;
            }
        }

        return usrIndex;
    }
</script>
