<div class="ted-header-image">
            <img src="images/header_image.png" width="100%" />

            <div class="ted-header-image-arrow">
                <a href="#">
                    <img src="images/header_arrow.png" />
                </a>
            </div>
        </div>

        <div class="container ted-symbol-box">
            <div class="row">
                <div class="col-md-5 ted-symbol-box-left">
                    <img src="images/tedx_symbol.png" />
                </div>
                <div class="col-md-7 ted-symbol-box-right">
                    <h1>Titolo</h1>
                    <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>
                </div>
            </div>
        </div>

        <div class="ted-grid">

        </div>

        <div class="ted-partners">

        </div>
